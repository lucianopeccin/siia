﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using XHouse.Model;

namespace XHouse.Data.Interfaces
{
    /// <summary>
    /// Define las operaciones comunes para todos los repositorios
    /// </summary>
    public interface IRepository<T> : IDisposable
    {
        T Get(int id);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();

        IQueryable<T> GetQueryable(int id);
        IQueryable<T> GetQueryable(Expression<Func<T, bool>> predicate);

        T Add(T model);
        T Update(T model);

        void Remove(int id);
        void Remove(T model);

        void Save();
    }
}
