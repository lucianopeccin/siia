﻿using System.Data.Entity;

namespace XHouse.Data.Interfaces
{
    public interface IDatabaseFactory
    {
        DbContext Get();
    }
}
