﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using XHouse.Data.Interfaces;
using XHouse.Model;
using XHouse.Model.Aparatos;

namespace XHouseWeb.Controllers
{
    public class ModulosController : Controller
    {
        private readonly IRepository<Modulo> _repositorioModulos;
        private readonly IRepository<Aparato> _repositorioAparatos;

        [Inject]
        public ModulosController(IRepository<Modulo> repModulos, IRepository<Aparato> repAparatos)
        {
            if (repModulos == null)
                throw new ArgumentNullException("repModulos");

            if (repModulos == null)
                throw new ArgumentNullException("repModulos");

            _repositorioModulos = repModulos;
            _repositorioAparatos = repAparatos;
        }


        //
        // GET: /Modulos/
        public ActionResult Index()
        {
            var modulos = _repositorioModulos.GetAll();
            return View(modulos.ToList());
        }

        //
        // GET: /Modulos/Details/5

        public ActionResult Details(int id = 0)
        {
            Modulo modulo = _repositorioModulos.Get(id);
            if (modulo == null)
            {
                return HttpNotFound();
            }

            // Incluyo los aparatos asociados al modulo, ya que voy a mostrar el detalle
            modulo.Aparatos = _repositorioAparatos.GetQueryable(a => a.ModuloId == id).Include(a => a.TipoAparato);
            
            return View(modulo);
        }

        //
        // GET: /Modulos/Create
        public ActionResult Create()
        {
            // Setear lo comboboxes
            return View();
        }

        //
        // POST: /Modulos/Create

        [HttpPost]
        public ActionResult Create(Modulo modulo)
        {
            if (!ModelState.IsValid)
                return View(modulo);

            // Chequeo que no haya otro modulo con el mismo nombre ya ingresado
            if (_repositorioModulos.Get(m => m.Nombre == modulo.Nombre).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe un Módulo con el nombre <b>{0}</b>.", modulo.Nombre));
                return View(modulo);
            }

            // Chequeo que no haya otro módulo con la misma dirección
            if (_repositorioModulos.Get(m => m.Direccion == modulo.Direccion).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe un Módulo la dirección <b>{0}</b>.", modulo.Direccion));
                return View(modulo);
            }

            // No hay errores en el modelo
            _repositorioModulos.Add(modulo);
            _repositorioModulos.Save();

            return RedirectToAction("Index");
        }

        //
        // GET: /Modulos/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Modulo modulo = _repositorioModulos.Get(id);

            if (modulo == null)
            {
                return HttpNotFound();
            }

            return View(modulo);
        }

        //
        // POST: /Modulos/Edit/5

        [HttpPost]
        public ActionResult Edit(Modulo modulo)
        {
            if (!ModelState.IsValid)
                return View(modulo);

            // Chequeo que no haya otro modulo con el mismo nombre ya ingresado
            if (_repositorioModulos.Get(m => m.Nombre == modulo.Nombre && m.Id != modulo.Id).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe un Módulo con el nombre <b>{0}</b>.", modulo.Nombre));
                return View(modulo);
            }

            // Chequeo que no haya otro módulo con la misma dirección
            if (_repositorioModulos.Get(m => m.Direccion == modulo.Direccion && m.Id != modulo.Id).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe un Módulo la dirección <b>{0}</b>.", modulo.Direccion));
                return View(modulo);
            }

            _repositorioModulos.Update(modulo);
            _repositorioModulos.Save();;
            return RedirectToAction("Index");
        }

        //
        // GET: /Modulos/Delete/5
        public ActionResult Delete(int id)
        {
            _repositorioModulos.Remove(id);
            _repositorioModulos.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _repositorioModulos.Dispose();
            base.Dispose(disposing);
        }

    }
}
