﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using XHouse.Data.Interfaces;
using XHouse.Model;

namespace XHouseWeb.Controllers
{
    public class HistorialController : Controller
    {
        protected IRepository<Historial> _repositorioHistorial; 

        public HistorialController(IRepository<Historial> repHistorial)
        {
            if (repHistorial == null)
                throw new ArgumentNullException("repHistorial");

            _repositorioHistorial = repHistorial;
        }
    

        //
        // GET: /Historial/
        public ActionResult Index()
        {
            var historialOrdenado = _repositorioHistorial.GetAll().OrderByDescending(h => h.Fecha);
            return View(historialOrdenado.ToList());
        }

        //
        // GET: /Historial/Details/5
        public ActionResult Details(int id)
        {
            var hist = _repositorioHistorial.Get(id);
            
            if (hist == null)
            {
                return HttpNotFound();
            }

            return View(hist);
        }
    }
}
