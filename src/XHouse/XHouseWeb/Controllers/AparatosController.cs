﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Ninject;
using XHouse.Data.Interfaces;
using XHouse.Model;
using XHouse.Model.Aparatos;

namespace XHouseWeb.Controllers
{
    public class AparatosController : Controller
    {
        private readonly IRepository<Aparato> _repositorioAparatos;
        private readonly IRepository<Zona> _repositorioZonas;
        private readonly IRepository<TipoAparato> _repositorioTipoAparatos;
        private readonly IRepository<Modulo> _repositorioModulos;
        private readonly IRepository<Ambiente> _repositorioAmbientes;
        private readonly IRepository<TipoAparato> _tipoAparato;

        [Inject]
        public AparatosController(IRepository<Aparato> repAparatos, IRepository<Zona> repZonas, IRepository<TipoAparato> repTipoAparatos, IRepository<Modulo> repModulos, IRepository<Ambiente> repAmbientes)
        {
            if (repAparatos == null)
                throw new ArgumentNullException("repAparatos");

            if (repZonas == null)
                throw new ArgumentNullException("repZonas");

            if (repTipoAparatos == null)
                throw new ArgumentNullException("repTipoAparatos");

            if (repTipoAparatos == null)
                throw new ArgumentNullException("repModulos");

            if (repTipoAparatos == null)
                throw new ArgumentNullException("repAmbientes");
            
            _repositorioAparatos = repAparatos;
            _repositorioZonas = repZonas;
            _repositorioTipoAparatos = repTipoAparatos;
            _repositorioModulos = repModulos;
            _repositorioAmbientes = repAmbientes;
        }


        //
        // GET: /Aparatos/
        public ActionResult Index()
        {
            var apar = _repositorioAparatos.GetAll();
            // TODO: Ver si es necesario levantar la zona asociada
            //aparatos.AsQueryable<Aparato>().Include(a => a.Zona);
            return View(apar.ToList());
        }

        //
        // GET: /Aparatos/Details/5

        public ActionResult Details(int id = 0)
        {
            Aparato aparato = _repositorioAparatos.Get(id);
            if (aparato == null)
            {
                return HttpNotFound();
            }
            return View(aparato);
        }

        //
        // GET: /Aparatos/Create
        public ActionResult Create()
        {
            ViewBag.Zonas = _repositorioZonas.GetAll();
            ViewBag.TiposAparatos = _repositorioTipoAparatos.GetAll();
            ViewBag.Modulos = _repositorioModulos.GetAll();
            ViewBag.Ambientes = _repositorioAmbientes.GetAll();
            return View();
        }

        //
        // POST: /Aparatos/Create

        [HttpPost]
        public ActionResult Create(Aparato aparato)
        {
            if (ModelState.IsValid)
            {
                _repositorioAparatos.Add(aparato);
                _repositorioAparatos.Save();

                return RedirectToAction("Index");
            }

            ViewBag.ZonaId = new SelectList(_repositorioZonas.GetAll(), "Id", "Nombre", aparato.ZonaId);
            return View(aparato);
        }

        //
        // GET: /Aparatos/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Aparato aparato = _repositorioAparatos.Get(id);
            if (aparato == null)
            {
                return HttpNotFound();
            }

            ViewBag.Zonas = _repositorioZonas.GetAll();
            ViewBag.TiposAparatos = _repositorioTipoAparatos.GetAll();
            ViewBag.Modulos = _repositorioModulos.GetAll();
            ViewBag.Ambientes = _repositorioAmbientes.GetAll();
            return View(aparato);
        }

        //
        // POST: /Aparatos/Edit/5

        [HttpPost]
        public ActionResult Edit(Aparato aparato)
        {
            if (ModelState.IsValid)
            {
                aparato.TipoAparato = _repositorioTipoAparatos.Get(aparato.TipoAparatoId);

                _repositorioAparatos.Update(aparato);
                _repositorioAparatos.Save();;
                return RedirectToAction("Index");
            }

            return View(aparato);
        }

        //
        // GET: /Aparatos/Delete/5
        public ActionResult Delete(int id)
        {
            _repositorioAparatos.Remove(id);
            _repositorioAparatos.Save();

            return RedirectToAction("Index");
        }

        // GET: /Aparatos/Get/1
        public JsonResult Get(int id)
        {
            var a = _repositorioAparatos.Get(id);
            dynamic result = new
                {
                    id = a.Id,
                    tipoId = a.TipoAparatoId,
                    nombre = a.Nombre,
                    numero = a.Numero,
                    habilitado = a.Habilitado,
                    estado = a.EstadoActual
                };

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        // POST: /Aparatos/Save
        [HttpPost]
        public JsonResult Save(Aparato a)
        {
            return null;
        }

        [HttpPost]
        public void Posicionar(Aparato posApar)
        {
            if (posApar.Id <= 0)
                return;

            var a = _repositorioAparatos.Get(posApar.Id);
            a.PosicionX = posApar.PosicionX;
            a.PosicionY = posApar.PosicionY;

            _repositorioAparatos.Update(a);
            _repositorioAparatos.Save();
        }

        protected override void Dispose(bool disposing)
        {
            _repositorioAparatos.Dispose();
            _repositorioZonas.Dispose();
            base.Dispose(disposing);
        }
    }
}