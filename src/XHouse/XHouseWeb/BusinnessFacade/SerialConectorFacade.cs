﻿using SerialConector;

namespace XHouseWeb.BusinnessFacade
{
    public class SerialConectorFacade : ISerialConectorFacade
    {
        private static ISerialConectorFacade _serialConector = new SerialConector.SerialConector();

        public bool PortIsOpen()
        {
            return _serialConector.PortIsOpen();
        }

        public bool StartListening()
        {
            return _serialConector.StartListening();
        }

        public bool StopListening()
        {
            return _serialConector.StopListening();
        }
    }
}