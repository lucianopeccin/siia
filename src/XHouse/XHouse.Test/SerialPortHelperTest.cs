﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using SerialPortListener;

namespace XHouse.Test
{
    [TestFixture]
    public class SerialPortHelperTest
    {
        private ISerialPortHelper _serialPortHelper;

        /// <summary>
        /// Inicializacion de test.
        /// Creacion de mocks.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            _serialPortHelper = new SerialPortHelper();

        }

        [Test]
        public void Detect18Testin()
        {
            var rdo = _serialPortHelper.DetectModule();
            Assert.True(rdo);
        }
    }
}
