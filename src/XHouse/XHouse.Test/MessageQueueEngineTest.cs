﻿using NUnit.Framework;

using Rhino.Mocks;

using SerialPortListener;

using XHouse.MessageQueueEngine;
using XHouse.MessageQueueEngine.Interfaces;

namespace XHouse.Test
{
    /// <summary>
    /// The message queue engine test.
    /// </summary>
     [TestFixture]
    public class MessageQueueEngineTest
    {
        /// <summary>
        /// The _sergial port helper.
        /// </summary>
        private ISerialPortHelper _serialPortHelper;

        private IInbox _inbox;

        private IOutbox _outbox;

        private IMessageManager _messageQueueEngine;


        /// <summary>
        /// Inicializacion de test.
        /// Creacion de mocks.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            _serialPortHelper = MockRepository.GenerateMock<ISerialPortHelper>();
            _inbox = MockRepository.GenerateMock<IInbox>();
            _outbox = MockRepository.GenerateMock<IOutbox>();


        }

        /// <summary>
        /// The message manager start comunication.
        /// </summary>
        ///[Test]
        public void MessageManagerStartComunication()
        {
            _serialPortHelper.Stub(m => m.DetectModule()).Return(true);
            this._messageQueueEngine = new MessageManager(_serialPortHelper,_inbox,_outbox);
           
           this._messageQueueEngine.StartComunication();

           _serialPortHelper.AssertWasCalled(m=>m.DetectModule());
            
       }

        [Test]
        public void MessageManagerStartComunicationTestIntegracion()
        {
           
            this._messageQueueEngine = new MessageManager(new SerialPortHelper(), new Inbox(), new Outbox());
            var rdo = this._messageQueueEngine.StartComunication();
            Assert.True(rdo);
        }
    }
}
