﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.ServiceRunner;

namespace ConsoleTestWS
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceRunner = new ServiceRunner();

            serviceRunner.Start();

            string str;
            while (true)
            {
                str = Console.ReadLine();

                if (str != null && str.ToUpperInvariant().Equals("EXIT"))
                {
                    serviceRunner.Stop();
                    return;
                }

                if (str != null && str.ToUpperInvariant().Equals("START18"))
                {
                    
                    return;
                }

                if (str != null && str.ToUpperInvariant().Equals("SINCRO"))
                {
                    serviceRunner.sincronizar();
                }
                if (str != null && str.ToUpperInvariant().Equals("SEND 00"))
                {
                    if (serviceRunner.write00())
                    {
                        Console.WriteLine("00 sended");
                    }
                }
                if (str != null && str.ToUpperInvariant().Equals("SEND"))
                {
                    Console.WriteLine("Ingrese el mensaje:");
                    var message = Console.ReadLine();
                    if (serviceRunner.write(message))
                    {
                        Console.WriteLine(message + " sended");
                    }
                    else
                    {
                        Console.WriteLine(message + " fail");
                    }
                }
            }
             
        }
    }
}
