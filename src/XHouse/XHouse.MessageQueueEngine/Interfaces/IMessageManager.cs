﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XHouse.MessageQueueEngine.Interfaces
{
    /// <summary>
    /// The MessageManager interface.
    /// </summary>
    public interface IMessageManager
    {
        /// <summary>
        /// The start comunication.
        /// </summary>
        bool StartComunication();

        /// <summary>
        /// The stop comunication.
        /// </summary>
        bool StopComunication();

        bool sincronizar();

        bool write00();

        bool write(string message);
    }
}
