﻿namespace XHouse.MessageQueueEngine.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using XHouse.Model.Message;

    /// <summary>
    /// The Inbox interface.
    /// </summary>
    public interface IInbox
    {
        /// <summary>
        /// The get next message.
        /// </summary>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        Message18 GetNextMessage();

        /// <summary>
        /// The get next message.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        Message18 GetNextMessage(string serial);

        /// <summary>
        /// The put message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool PutMessage(Message18 message);

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ClearQueue(string serial);

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ClearQueue();

        /// <summary>
        /// The new message.
        /// </summary>
        event EventHandler NewMessage;
    }
}
