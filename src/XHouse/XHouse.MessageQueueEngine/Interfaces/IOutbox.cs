﻿using System;

using XHouse.Model.Message;

namespace XHouse.MessageQueueEngine.Interfaces
{
    /// <summary>
    /// The Outbox interface.
    /// </summary>
    public interface IOutbox
    {
        /// <summary>
        /// The get next message.
        /// </summary>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        Message18 GetNextMessage();

        /// <summary>
        /// The get next message.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        Message18 GetNextMessage(string serial);

        /// <summary>
        /// The put message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool PutMessage(Message18 message);

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ClearQueue(string serial);

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ClearQueue();

        /// <summary>
        /// The new message.
        /// </summary>
        event EventHandler NewMessage;
    }
}
