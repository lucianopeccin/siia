﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Outbox.cs" company="">
//   
// </copyright>
// <summary>
//   The outbox.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace XHouse.MessageQueueEngine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using XHouse.MessageQueueEngine.Interfaces;
    using XHouse.Model.Message;

    /// <summary>
    /// The outbox.
    /// </summary>
    public class Outbox : IOutbox
    {
        /// <summary>
        /// The new message.
        /// </summary>
        public event EventHandler NewMessage;

        private List<Message18> _outbox;

        public Outbox()
        {
            _outbox = new List<Message18>();
        }

        /// <summary>
        /// The get next message.
        /// </summary>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Message18 GetNextMessage()
        {
            if (_outbox != null)
            {
                var rdo = _outbox.First();
                _outbox.Remove(rdo);
                Console.WriteLine("Mensaje saliente: " + rdo.allData);
                return rdo;
            }

            return null;
        }

        /// <summary>
        /// The get next message.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Message18 GetNextMessage(string serial)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The put message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool PutMessage(Message18 message)
        {
            if (message != null)
            {
                _outbox.Add(message);
                EventHandler handler = NewMessage;
                if (handler != null)
                {
                    handler(this, null);

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool ClearQueue(string serial)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool ClearQueue()
        {
            throw new NotImplementedException();
        }
    }
}
