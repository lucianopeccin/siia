﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.MessageQueueEngine.Interfaces;
using XHouse.Model.Message;

namespace XHouse.MessageQueueEngine
{
    /// <summary>
    /// The inbox.
    /// </summary>
    public class Inbox : IInbox
    {
        /// <summary>
        /// The new message.
        /// </summary>
        public event EventHandler NewMessage;

        private List<Message18> _inbox;

        public Inbox()
        {
            _inbox = new List<Message18>();
        }
        /// <summary>
        /// The get next message.
        /// </summary>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Message18 GetNextMessage()
        {
            if (_inbox != null)
            {
                var rdo = _inbox.First();
                _inbox.Remove(rdo);
                return rdo;
            }

            return null;
        }

        /// <summary>
        /// The get next message.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="Message18"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public Message18 GetNextMessage(string serial)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The put message.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool PutMessage(Message18 message)
        {
            if (this._inbox != null)
            {
                this._inbox.Add(message);
                Console.WriteLine("Mensaje Entrante: " + message.allData);

                if (NewMessage != null)
                {
                    NewMessage(this, new EventArgs());

                    return true;
                }
                

            }

            return false;
        }

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <param name="serial">
        /// The serial.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool ClearQueue(string serial)
        {
            if (this._inbox != null)
            {
                foreach (var message18 in _inbox)
                {
                    if (message18.serie.Equals(serial))
                    {
                        _inbox.Remove(message18);
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// The clear queue.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool ClearQueue()
        {
            if (this._inbox != null)
            {
                this._inbox.Clear();
                return true;
            }

            return false;
        }
    }
}
