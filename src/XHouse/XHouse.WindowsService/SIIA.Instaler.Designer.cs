﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SIIA.Instaler.Designer.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the Installer1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace SIIAWindowsService
{
    /// <summary>
    /// The installer 1.
    /// </summary>
    public partial class Installer1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Instalador de servicios.
        /// </summary>
        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstaller1;

        /// <summary>
        /// Servicio a instalar.
        /// </summary>
        private System.ServiceProcess.ServiceInstaller serviceInstaller1;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();

            this.serviceProcessInstaller1 = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstaller1 = new System.ServiceProcess.ServiceInstaller();

            // serviceProcessInstaller1
            this.serviceProcessInstaller1.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.serviceProcessInstaller1.Password = null;
            this.serviceProcessInstaller1.Username = null;

            // serviceInstaller1
            this.serviceInstaller1.Description = "Servicio windows que permite lanzar en paralelo N tareas configurables para ejecu" +
                "tarse periodicamente.";
            this.serviceInstaller1.DisplayName = "SIIA Domotic System";
            this.serviceInstaller1.ServiceName = "SIIA.WindowsService";

            // ProjectInstaller
            this.Installers.AddRange(new System.Configuration.Install.Installer[] { this.serviceProcessInstaller1, this.serviceInstaller1 });
         }

        #endregion
    }
}