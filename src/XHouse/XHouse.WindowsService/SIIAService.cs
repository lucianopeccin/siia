﻿using System.ServiceProcess;

using XHouse.ServiceRunner;

namespace XHouse.WindowsService
{
    public partial class SIIAService : ServiceBase
    {
        /// <summary>
        /// The service runner.
        /// </summary>
        private IServiceRunner serviceRunner = new ServiceRunner.ServiceRunner();

        /// <summary>
        /// Initializes a new instance of the <see cref="SIIAService"/> class.
        /// </summary>
        public SIIAService()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The on start.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        protected override void OnStart(string[] args)
        {
            serviceRunner.Start();

        }

        /// <summary>
        /// The on stop.
        /// </summary>
        protected override void OnStop()
        {
            serviceRunner.Stop();
        }
    }
}
