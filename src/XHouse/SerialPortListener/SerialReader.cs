﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SerialPortListener.Serial;

namespace SerialPortListener
{
   public class SerialReader
    {
         SerialPortManager _spManager;
        public SerialReader()
        {
            UserInitialization();
        }

       public bool IsPortOpen()
       {
           return _spManager.PortIsOpen();
       }
      
        private void UserInitialization()
        {
            _spManager = new SerialPortManager();
            SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
          
            _spManager.NewSerialDataRecieved += new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved);
           
        }

        
        void _spManager_NewSerialDataRecieved(object sender, SerialDataEventArgs e)
        {
           string str = Encoding.ASCII.GetString(e.Data);
            Console.WriteLine(str);

        }

        
        public void StartListening()
        {
            _spManager.StartListening();
        }

        
        public void StopListening()
        {
            _spManager.StopListening();
        }
    }
}
