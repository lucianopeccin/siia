﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialPortListener
{
    public interface ISerialPortHelper
    {
        bool DetectModule();

        byte[] StringToByteArray(string hex);

        bool ClosePort();

        string ReadExisting();

        event EventHandler Lectura;

        bool WritePort(String mBuffer);

        bool WritePortString(string mbuffer);

        bool sincronizar();
    }
}
