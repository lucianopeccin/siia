﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialPortListener
{
    using System.IO.Ports;

    /// <summary>
    /// The serial port helper.
    /// </summary>
    public class SerialPortHelper : ISerialPortHelper
    {
        // Add this variable

        public  string RxString;
        public event EventHandler Lectura;

        private bool sincronizado = false;

        /// <summary>
        /// Escribe el mBuffer en el puerto Serie conectado al 18.
        /// </summary>
        /// <param name="mBuffer">
        /// The m buffer.
        /// </param>
        /// <returns>
        /// True si no paso nada, false en caso de error.
        /// </returns>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public bool WritePort(byte[] mBuffer)
        {
            if (!this.serialPort1.IsOpen)
            {
                return false;
            }

            this.serialPort1.Write(mBuffer, 0, mBuffer.Length);

            return true;
        }

        public bool WritePort(string mBuffer)
        {
            return this.WritePortString(mBuffer);
        }

        public bool WritePortString(String mBuffer)
        {
            if (!this.ValidateArray(mBuffer))
            {
                return false;
            }

            //if (mBuffer.Length != 14)
            //{
            //    return false;
            //}

            return WritePort(StringToByteArray(mBuffer));
        }

        SerialPort serialPort1 = new SerialPort();

        public bool DetectModule()
        {
            Console.WriteLine("Conectando . . .");
            serialPort1.DataReceived += new SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            var txt = string.Empty;
            var flag = false;
            int i = 1;
            while (!flag && i < 20)
            {
                try
                {
                    serialPort1.Close();
                    System.Threading.Thread.Sleep(500);
                    serialPort1.PortName = "COM" + i.ToString();
                    txt = "Traying Opening Port" + serialPort1.PortName;
                    serialPort1.BaudRate = 9600;
                    i++;
                    serialPort1.Open();
                    if (serialPort1.IsOpen)
                    {
                        flag = Comprobar18();
                        if (flag)
                        {
                            Console.WriteLine("Conectado - Port Detected: " + serialPort1.PortName + " BaudRate: " + serialPort1.BaudRate + "Por Open: " + serialPort1.IsOpen);
                            return true;
                        }
                    }
                }
                catch (System.IO.IOException exception)
                {
                    //el puerto probado no existe...solo continuo buscando
                }
            }

            Console.WriteLine("Fallo la conexión");

            return false;

        }

        byte[] ISerialPortHelper.StringToByteArray(string hex)
        {
            return StringToByteArray(hex);
        }


        /// <summary>
        /// The comprobar 18.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool Comprobar18()
        {
            if (!serialPort1.IsOpen) return false;

            byte[] mBuffer2 = StringToByteArray("aaaa00aabbccdd");

            serialPort1.Write(mBuffer2, 0, mBuffer2.Length);

            System.Threading.Thread.Sleep(500);

            var flag = !String.IsNullOrEmpty(this.ReadExisting());

            if (!flag)
            {
                flag = sincronizar();                
            }

            return flag;
        }

        public bool sincronizar()
        {
            var flag = false;
            int i = 0;
            var read = "";
            while (!flag && i < 30 && serialPort1.IsOpen && !sincronizado)
            {
                byte[] mbufferSincro = StringToByteArray("00");
                serialPort1.Write(mbufferSincro, 0, mbufferSincro.Length);

                System.Threading.Thread.Sleep(500);
                read = ReadExisting();
                flag = !String.IsNullOrEmpty(read);

                i++;
            }

            if (sincronizado)
            {
                flag = sincronizado;}

            Console.WriteLine("Sincronización: " + flag + " - Message: " +read );

            sincronizado = false;
            return flag; 
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public bool ClosePort()
        {
            if (serialPort1.IsOpen)
            {
                serialPort1.Close();
                return true;
            }

            return false;
        }

        /// <summary>
        /// The serial port 1_ data received.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void serialPort1_DataReceived
          (object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            
            //RxString = "";
            if (serialPort1.IsOpen)
            {
                
                RxString += serialPort1.ReadExisting();

                if (RxString.Length >= 14)
                {

                    if (RxString.Equals("0000ff00000000"))
                    {
                        sincronizado = true;
                    }

                    if (Lectura != null)
                    {
                        Lectura(this, new EventArgs());
                    } 
                }
            }

        }

        /// <summary>
        /// The read existing.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string ReadExisting()
        {
            if (String.IsNullOrEmpty(this.RxString))
            {
                return "";
            }
            var msg = this.RxString.Substring(0, 14);

            if (this.RxString.Length >= 14)
            {
                this.RxString = this.RxString.Substring(13, this.RxString.Length - 14);    
            }
            return msg;
        }

        private bool ValidateArray(string aBytes)
        {
            foreach (var aByte in aBytes)
            {
                if (!this.validateCaracterHexa(Convert.ToChar(aByte)))
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// The validate caracter hexa.
        /// </summary>
        /// <param name="a">
        /// The a.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool validateCaracterHexa(char a)
        {
            if (a == 'a' || a == 'b' || a == 'c' || a == 'd' || a == 'e' || a == 'f' || a == '0' || a == '1' || a == '2'
                || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9')
            {
                return true;
            }
            return false;
        }
    }
}
