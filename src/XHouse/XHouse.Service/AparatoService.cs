﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.Data.Interfaces;
using XHouse.Model.Aparatos;
using XHouse.Service.Interface;

namespace XHouse.Service
{
   public  class AparatoService : IAparatoService
   {
       private readonly IRepository<Aparato> _repository;

       public AparatoService(IRepository<Aparato> repository)
       {
           _repository = repository;
       }

       public Aparato GetAparato(string module, string port, string aparato)
       {
         return _repository.Get(x => x.Modulo.Direccion == module && x.Puerto == port && x.Posicion == aparato).First();
       }
   }
}
