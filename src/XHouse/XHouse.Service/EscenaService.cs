﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.Data.Interfaces;
using XHouse.Model.Escenas;
using XHouse.Service.Interface;

namespace XHouse.Service
{
    public class EscenaService : IEscenaService
    {
        private readonly IRepository<Escena> _repositoryEscena;

        private readonly IRepository<EscenaAparatoEstado> _repositoryEscenaAparatoEstado;

        public EscenaService(IRepository<Escena> repositoryEscena, IRepository<EscenaAparatoEstado> repositoryEscenaAparatoEstado )
        {
            _repositoryEscena = repositoryEscena;
            _repositoryEscenaAparatoEstado = repositoryEscenaAparatoEstado;
        }

        public List<Escena> GetAllEscenas()
        {
            return _repositoryEscena.GetAll().ToList();
        } 

    }
}
