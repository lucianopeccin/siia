﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.Model.Aparatos;

namespace XHouse.Service.Interface
{
    public interface IAparatoService
    {
        Aparato GetAparato(string module, string port, string aparato);
    }
}
