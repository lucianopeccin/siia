﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.Model.Escenas;

namespace XHouse.Service.Interface
{
    public interface IEscenaService
    {
        List<Escena> GetAllEscenas();
    }
}
