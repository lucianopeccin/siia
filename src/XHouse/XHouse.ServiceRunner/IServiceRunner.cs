﻿namespace XHouse.ServiceRunner
{
    /// <summary>
    /// Interfaz de la clase principal, que instancia y
    /// ejecuta todos los servicios configurados.
    /// </summary>
    public interface IServiceRunner
    {
        /// <summary>
        /// Instancia todos los servicios configurados y
        /// sus depedencias e inicia la ejecución de cada uno.
        /// </summary>
        void Start();

        /// <summary>
        /// Para todos los servicios, elimina
        /// todas las instancias creadas y libera
        /// los recursos utilizados por los servicios.
        /// </summary>
        void Stop();
    }
}
