﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using Ninject.Web.Common;

using XHouse.Data;
using XHouse.Data.Interfaces;
using XHouse.IoC;
using XHouse.MessageQueueEngine;
using XHouse.MessageQueueEngine.Interfaces;

namespace XHouse.ServiceRunner
{
    /// <summary>
    /// The service runner.
    /// </summary>
    public class ServiceRunner : IServiceRunner
    {
        /// <summary>
        /// The bootstrapper.
        /// </summary>
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        private IMessageManager _messageManager;
       
        

        /// <summary>
        /// The start.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Start()
        {
            bootstrapper.Initialize(CreateKernel);

            var _messageManager = bootstrapper.Kernel.Get<IMessageManager>();
            var _engineMessage = bootstrapper.Kernel.Get<IEngineMessage>();

            _messageManager.StartComunication();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
           
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
           kernel.Load(new XHouseModule());

           //IDatabaseFactory databaseFactory = new DatabaseFactory();

           //kernel.Bind(typeof(IDatabaseFactory)).ToConstant(databaseFactory);

           //kernel.Bind(typeof(IRepository<>))
           //      .To(typeof(Repository<>))
           //      .WithConstructorArgument("dbFactory", databaseFactory);
        }

        /// <summary>
        /// The stop.
        /// </summary>
        /// <exception cref="NotImplementedException">
        /// </exception>
        public void Stop()
        {
            bootstrapper.ShutDown();
        }

        public bool sincronizar()
        {
            if (_messageManager == null)
            {
                 _messageManager = bootstrapper.Kernel.Get<IMessageManager>();
            }
            if (_messageManager != null)
            {
                return _messageManager.sincronizar();
            }

            Console.WriteLine("No se pudo sincronizar");
            return false;
        }

        public bool write00()
        {
            if (_messageManager == null)
            {
                _messageManager = bootstrapper.Kernel.Get<IMessageManager>();
            }
            if (_messageManager != null)
            {
                return _messageManager.write00();
            }

            Console.WriteLine("No se pudo sincronizar");
            return false;
        }

        public bool write(string message)
        {
            if (_messageManager == null)
            {
                _messageManager = bootstrapper.Kernel.Get<IMessageManager>();
            }
            if (_messageManager != null)
            {
                return _messageManager.write(message);
            }

            Console.WriteLine("No se pudo enviar el mensaje");
            return false;
        }
    }
}
