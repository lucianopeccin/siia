﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.Model.Message;

namespace XHouse.BusinessLayer.Interface
{
    public interface IMessageProcessor
    {
        List<Message18> Process(Message18 message18);
    }
}
