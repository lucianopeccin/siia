﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using XHouse.BusinessLayer.Interface;
using XHouse.Model.Escenas;
using XHouse.Model.Message;
using XHouse.Service.Interface;

namespace XHouse.BusinessLayer
{
    public class MessageProcessor : IMessageProcessor
    {
        private readonly IEscenaService _escenaService;

        private readonly IAparatoService _aparatoService;

        public MessageProcessor(IEscenaService escenaService, IAparatoService aparatoService)
        {
            _escenaService = escenaService;

            _aparatoService = aparatoService;
        }

        public List<Message18> Process(Message18 message18)
        {

            if (message18.header.Equals("ff")) return null; //Es un mensaje de Weakup

            var rdo = new List<Message18>();

            message18.data = "01";
            rdo.Add(message18);

            var escenasActivas = this.GetEscenasActivas();

            /*Cosas por hacer
            Pedir todas las escenas
            si la escena no está activa ver si el mensaje la dispara
            si la escena está activa ver si este mensaje tiene salidas asociadas*/

            //escenasActivas[1].

            //var aparato = _aparatoService.GetAparato(message18.module, message18.port, message18.aparato);

            return rdo;
        }

        private List<Escena> GetEscenasActivas()
        {
            return _escenaService.GetAllEscenas().Where(x => x.Activa).ToList();
        }

        private bool FindAparato(Escena escena, Message18 message18)
        {
            var aparato = _aparatoService.GetAparato(message18.module, message18.port, message18.aparato);

            return true;
        }
    }
}
