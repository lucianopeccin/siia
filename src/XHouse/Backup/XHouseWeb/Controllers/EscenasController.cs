﻿using System;
using System.Web.Mvc;
using Ninject;
using XHouse.Data.Interfaces;
using XHouse.Model;
using XHouse.Model.Escenas;
using XHouseWeb.Helpers;

namespace XHouseWeb.Controllers
{
    public class EscenasController : Controller
    {
        private readonly IRepository<Escena> _repositorio;
        

        [Inject]
        public EscenasController(IRepository<Escena> repository, ImageProvider imageProvider)
        {
            if (repository == null)
                throw new ArgumentNullException("repository");

            if (imageProvider == null)
                throw new ArgumentNullException("imageProvider");

            _repositorio = repository;
        }


        //
        // GET: /Escenas/

        public ActionResult Index()
        {
            return View(_repositorio.GetAll());
        }

        //
        // GET: /Escenas/Details/5

        public ActionResult Details(int id = 0)
        {
            Escena escena = _repositorio.Get(id);
            if (escena == null)
            {
                return HttpNotFound();
            }
            return View(escena);
        }

        //
        // GET: /Escenas/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Escenas/Create

        [HttpPost]
        public ActionResult Create(Escena escena)
        {
            if (ModelState.IsValid)
            {
                _repositorio.Add(escena);
                _repositorio.Save();
                return RedirectToAction("Index");
            }

            return View(escena);
        }

        //
        // GET: /Escenas/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Escena escena = _repositorio.Get(id);
            if (escena == null)
            {
                return HttpNotFound();
            }
            return View(escena);
        }

        //
        // POST: /Escenas/Edit/5

        [HttpPost]
        public ActionResult Edit(Escena escena)
        {
            if (ModelState.IsValid)
            {
                _repositorio.Update(escena);
                _repositorio.Save();

                return RedirectToAction("Index");
            }
            return View(escena);
        }

        //
        // GET: /Escenas/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Escena escena = _repositorio.Get(id);
            if (escena == null)
            {
                return HttpNotFound();
            }
            return View(escena);
        }

        //
        // POST: /Escenas/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            _repositorio.Remove(id);
            _repositorio.Save();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _repositorio.Dispose();
            base.Dispose(disposing);
        }
    }
}