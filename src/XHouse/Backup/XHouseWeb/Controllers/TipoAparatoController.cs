﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Ninject;
using XHouse.Data.Interfaces;
using XHouse.Model;
using XHouse.Model.Aparatos;
using XHouseWeb.Helpers;


namespace XHouseWeb.Controllers
{
    public class TipoAparatosController : Controller
    {
        private readonly IRepository<TipoAparato> _repositorioTipoAparato;
        private readonly ImageProvider _imageProvider;

        [Inject]
        public TipoAparatosController(IRepository<TipoAparato> repTipoAparato, ImageProvider imageProvider)
        {
            if (repTipoAparato == null)
                throw new ArgumentNullException("repTipoAparato");

            if (imageProvider == null)
                throw new ArgumentNullException("imageProvider");

            _imageProvider = imageProvider;
            _repositorioTipoAparato = repTipoAparato;
        }


        //
        // GET: /TipoAparatos/
        public ActionResult Index()
        {
            var tipoAparatos = _repositorioTipoAparato.GetAll();
            return View(tipoAparatos.ToList());
        }

        //
        // GET: /TipoAparatos/Details/5

        public ActionResult Details(int id = 0)
        {
            TipoAparato tipoAparato = _repositorioTipoAparato.Get(id);
            if (tipoAparato == null)
            {
                return HttpNotFound();
            }
            return View(tipoAparato);
        }

        //
        // GET: /TipoAparatos/Create
        public ActionResult Create()
        {
            // Setear lo comboboxes
            return View();
        }

        //
        // POST: /TipoAparatos/Create

        [HttpPost]
        public ActionResult Create(TipoAparato tipoAparato)
        {
            if (!ModelState.IsValid)
                return View(tipoAparato);

            // Chequeo que no haya otro aparato con el mismo nombre ya ingresado
            if (_repositorioTipoAparato.Get(ta => ta.Nombre == tipoAparato.Nombre).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe un Tipo de Aparato con el nombre {0}.", tipoAparato.Nombre));
                return View(tipoAparato);
            }

            // No hay errores en el modelo
            _repositorioTipoAparato.Add(tipoAparato);
            _repositorioTipoAparato.Save();

            return RedirectToAction("Index");
        }

        //
        // GET: /TipoAparatos/Edit/5
        public ActionResult Edit(int id = 0)
        {
            TipoAparato tipoAparato = _repositorioTipoAparato.Get(id);
            if (tipoAparato == null)
            {
                return HttpNotFound();
            }
           
            return View(tipoAparato);
        }

        //
        // POST: /TipoAparatos/Edit/5

        [HttpPost]
        public ActionResult Edit(TipoAparato tipoAparato)
        {
            if (!ModelState.IsValid)
                return View(tipoAparato);

            // Chequeo que no haya otro aparato con el mismo nombre ya ingresado
            if (_repositorioTipoAparato.Get(ta => ta.Nombre == tipoAparato.Nombre).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe un Tipo de Aparato con el nombre {0}.", tipoAparato.Nombre));
                return View(tipoAparato);
            }

            _repositorioTipoAparato.Update(tipoAparato);
            _repositorioTipoAparato.Save();;
            return RedirectToAction("Index");
        }

        //
        // GET: /TipoAparatos/Delete/5
        public ActionResult Delete(int id)
        {
            _repositorioTipoAparato.Remove(id);
            _repositorioTipoAparato.Save();

            return RedirectToAction("Index");
        }

        // GET: /TipoAparatos/Get
        public JsonResult Get(int id)
        {
            var ta = _repositorioTipoAparato.Get(id);
            dynamic result = new
                {
                    id = ta.Id,
                    nombre = ta.Nombre,
                    operacion = ta.Operacion.ToString(),
                    puedeApagarse = ta.PuedeApagarse,
                    estadoApagado = ta.EstadoApagado,
                    topeInferior = ta.RangoTopeInferior,
                    topeSuperior = ta.RangoTopeSuperior
                };
            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            _repositorioTipoAparato.Dispose();
            base.Dispose(disposing);
        }

    }

}
