﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ninject;
using XHouse.Data.Interfaces;
using XHouse.Model;
using XHouseWeb.Helpers;

namespace XHouseWeb.Controllers
{
    public class ZonasController : Controller
    {
        private readonly IRepository<Zona> _repositorio;
        private readonly ImageProvider _imageProvider;
        
        [Inject]
        public ZonasController(IRepository<Zona> repository, ImageProvider imageProvider)
        {
            if (repository == null)
                throw new ArgumentNullException("repository");

            if (imageProvider == null)
                throw new ArgumentNullException("imageProvider");

            _imageProvider = imageProvider;
            _repositorio = repository;
        }

        //
        // GET: /Zonas/

        public ActionResult Index()
        {
            return View(_repositorio.GetAll());
        }

        //
        // GET: /Zonas/Details/5

        public ActionResult Details(int id = 0)
        {
            Zona zona = _repositorio.Get(id);
            if (zona == null)
            {
                return HttpNotFound();
            }
            return View(zona);
        }

        //
        // GET: /Zonas/Create
        public ActionResult Create()
        {
            return View();
        }

        
        //
        // POST: /Zonas/Create
        [HttpPost]
        public ActionResult Create(Zona zona)
        {
            //if (ModelState.IsValid)
            //{
            //    _repositorio.Add(zona);
            //    _repositorio.Save();

            //    return RedirectToAction("Index");
            //}

            //return View(zona);
            if (!ModelState.IsValid)
                return View(zona);

            if (_repositorio.Get(m => m.Nombre == zona.Nombre).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe una Zona con el nombre <b>{0}</b>.", zona.Nombre));
                return View(zona);
            }

            // No hay errores en el modelo
            _repositorio.Add(zona);
            _repositorio.Save();

            return RedirectToAction("Index");
        }

        //
        // GET: /Zonas/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Zona zona = _repositorio.Get(id);
            if (zona == null)
            {
                return HttpNotFound();
            }
            return View(zona);
        }

        //
        // POST: /Zonas/Edit/5

        [HttpPost]
        public ActionResult Edit(Zona zona)
        {
            if (!ModelState.IsValid)
                return View(zona);

            // Chequeo que no haya otro zona con el mismo nombre ya ingresado
            if (_repositorio.Get(m => m.Nombre == zona.Nombre && m.Id != zona.Id).Any())
            {
                ModelState.AddModelError(string.Empty, string.Format("Ya existe una Zona con el nombre <b>{0}</b>.", zona.Nombre));
                return View(zona);
            }

            _repositorio.Update(zona);
            _repositorio.Save();
            return RedirectToAction("Index");
        }

        //
        // GET: /Zonas/Delete/5

        public ActionResult Delete(int id)
        {
            Zona zona = _repositorio.Get(id);
            if (zona == null)
            {
                return HttpNotFound();
            }
            else
            {
                _repositorio.Remove(id);
                _repositorio.Save();
                return RedirectToAction("Index");
            }
        }

        //
        // GET: /Escenas/Configure
        public ActionResult Configure(int id)
        {
            Zona zona = _repositorio.Get(id);

            // Imagen de la zona
            ViewBag.PathImagen = Url.Content(_imageProvider.GetImagePath(ImagenTipoObjeto.Ambiente, zona.Id));

            // Imagenes de los tipos de aparatos involucrados en la zona
            var pathImgTipoAparatos = new Dictionary<int, string>();

            foreach (var a in zona.Aparatos)
            {
                if (pathImgTipoAparatos.ContainsKey(a.TipoAparatoId))
                    continue;

                pathImgTipoAparatos.Add(a.TipoAparatoId,
                                        Url.Content(_imageProvider.GetImagePath(ImagenTipoObjeto.TipoAparato,
                                                                                a.TipoAparatoId)));
            }

            ViewBag.PathImgTipoAparatos = pathImgTipoAparatos;

            return View(zona);
        }

        protected override void Dispose(bool disposing)
        {
            _repositorio.Dispose();
            base.Dispose(disposing);
        }
    }
}