using XHouse.Data;
using XHouse.Data.Interfaces;
using XHouseWeb.Helpers;

[assembly: WebActivator.PreApplicationStartMethod(typeof(XHouseWeb.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethodAttribute(typeof(XHouseWeb.App_Start.NinjectWebCommon), "Stop")]

namespace XHouseWeb.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using SerialPortListener;

    

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
            
            RegisterServices(kernel);
            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //kernel.Load(Assembly.GetExecutingAssembly());

            IDatabaseFactory databaseFactory = new DatabaseFactory();

            kernel.Bind(typeof (IDatabaseFactory)).ToConstant(databaseFactory);

            kernel.Bind(typeof (IRepository<>))
                  .To(typeof (Repository<>))
                  .WithConstructorArgument("dbFactory", databaseFactory);

            kernel.Bind(typeof (ImageProvider)).To(typeof (ImageProvider)).InSingletonScope();
            kernel.Bind(typeof(SerialPortHelper)).To(typeof(SerialPortHelper)).InSingletonScope();
            
        }
    }
}
