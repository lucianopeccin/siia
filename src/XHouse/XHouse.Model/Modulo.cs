﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XHouse.Model.Aparatos;

namespace XHouse.Model
{
    [Table("Modulo")]
    public class Modulo : Identity
    {
        [Required(ErrorMessage = "El nombre del módulo es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El Nombre no puede superar los 50 caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [MaxLength(300, ErrorMessage = "La Descripción no puede superar los 300 caracteres.")]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "La dirección del módulo es obligatoria.")]
        [Display(Name = "Dirección")]
        [MaxLength(20, ErrorMessage = "La dirección del módulo no puede ser mayor a 20 caracteres.")]
        public string Direccion { get; set; }

        [Display(Name = "Ubicación")]
        [MaxLength(100, ErrorMessage = "La ubicación del módulo no puede ser mayor a 100 caracteres.")]
        public string Ubicacion { get; set; }

        [Required(ErrorMessage = "El tipo del módulo es obligatorio.")]
        [Display(Name = "Tipo")]
        public TipoModulo Tipo { get; set; }

        public IEnumerable<Aparato> Aparatos { get; set; }
    }

    /// <summary>
    /// Enumerado que indica el Tipo de Modulo.
    /// El tipo de modulo indica la cantidad de entradas y salidas.
    /// FIXME: Cada tipo de módulo tiene sus limitaciones, hay que hacer validaciones en el controlador
    /// </summary>
    public enum TipoModulo
    {
        /// <summary>
        /// Modulo primario (18F4550)
        /// </summary>
        Primario = 0,

        /// <summary>
        /// Modulo secundario (16F870)
        /// </summary>
        Secundario = 1
    }
}