﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XHouse.Model.Aparatos;

namespace XHouse.Model
{
    [Table("Ambiente")]
    public class Ambiente : Identity
    {
        [Required(ErrorMessage = "El Nombre es obligatorio.")]
        [MaxLength(50, ErrorMessage = "El nombre no puede superar los 50 caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [MaxLength(300, ErrorMessage = "La descripción no puede superar los 300 caracteres.")]
        public string Descripcion { get; set; }

        public virtual ICollection<Aparato> Aparatos { get; set; }
    }
}
