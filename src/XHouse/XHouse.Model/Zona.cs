﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XHouse.Model.Aparatos;

namespace XHouse.Model
{
    [Table("Zona")]
    public class Zona : Identity
    {
        [Required(ErrorMessage ="El campo Nombre es requerido.")]
        [MaxLength(50, ErrorMessage="El nombre no puede superar los 50 caracteres.")]
        public string Nombre { get; set; }

        [Display(Name="Descripción")]
        [Required(ErrorMessage = "El campo Descripción es requerido.")]
        [MaxLength(300, ErrorMessage = "La descripción no puede superar los 300 caracteres.")]
        public string Descripcion { get; set; }

        public virtual ICollection<Aparato> Aparatos { get; set; }
    }
}