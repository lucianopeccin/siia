﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XHouse.Model.Escenas
{
    public abstract class EscenaTrigger : Identity
    {
        /// <summary>
        /// Id de la Escena a la que pertenece el Trigger
        /// </summary>
        [Required(ErrorMessage = "La Escena asociada al Trigger es obligatoria")]
        public int EscenaId { get; set; }

        /// <summary>
        /// Escena a la que pertenece el Trigger
        /// </summary>
        public virtual Escena Escena { get; set; }

        public bool Habilitado { get; set; }
    }
}
