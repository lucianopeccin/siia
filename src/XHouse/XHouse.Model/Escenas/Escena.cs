﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHouse.Model.Escenas
{
    [Table("Escena")]
    public class Escena : Identity
    {
        [Required(ErrorMessage = "El campo Nombre es requerido.")]
        [MaxLength(50, ErrorMessage = "El Nombre no puede superar los 50 caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [MaxLength(300, ErrorMessage = "La Descripción no puede superar los 300 caracteres.")]
        public string Descripcion { get; set; }

        [Display(Name = "Activa")]
        public bool Activa { get; set; }

        /// <summary>
        /// Triggers de la Escena asociados a aparatos
        /// </summary>
        public virtual ICollection<EscenaAparatoTrigger> AparatoTriggers { get; set; }

        /// <summary>
        /// Triggers de la Escena asociados a timers temporales
        /// </summary>
        /// EscenaTimerTrigger
        public virtual ICollection<EscenaTimerTrigger> TimerTriggers { get; set; }

        /// <summary>
        /// Aparatos y los estados de los mismos para cuando se activa la Escena
        /// </summary>
        /// EscenaAparatoEstado
        public virtual ICollection<EscenaAparatoEstado> EstadoAparatos { get; set; }
    }
}