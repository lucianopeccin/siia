﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using XHouse.Model.Aparatos;

namespace XHouse.Model.Escenas
{
    /// <summary>
    /// Clase que representa cuando un estado específico de un Aparato lanza una Escena.
    /// </summary>
    [Table("EscenaAparatoTrigger")]
    public class EscenaAparatoTrigger : EscenaTrigger
    {
        /// <summary>
        /// Id del Aparato asociado al Trigger
        /// </summary>
        [Required(ErrorMessage = "El Aparato asociado al Trigger es obligatoria")]
        public int AparatoId { get; set; }

        /// <summary>
        /// Aparato asociado al Trigger
        /// </summary>
        public virtual Aparato Aparato { get; set; }

        /// <summary>
        /// Estado del aparato en el que se acciona el Trigger
        /// </summary>
        [Required(ErrorMessage = "El estado en el se dispara el Trigger es obligatoria")]
        public int Estado { get; set; }
    }
}
