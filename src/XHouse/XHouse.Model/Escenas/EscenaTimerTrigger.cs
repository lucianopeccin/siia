﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHouse.Model.Escenas
{
    /// <summary>
    /// Clase que representa cuando se lanza una Escena a un horario o día determinado.
    /// </summary>
    [Table("EscenaTimerTrigger")]
    public class EscenaTimerTrigger : EscenaTrigger
    {
        [MaxLength(5, ErrorMessage = "La Hora no puede tener mas de 5 caracteres.")]
        public string HoraFija { get; set; } // 11:23

        [MaxLength(7, ErrorMessage = "El campo Dias no puede tener mas de 7 caracteres.")]
        public string Dias { get; set; } // LMWJVSD

        public int CantidadPeriodo { get; set; }
        public Periodicidad Periodo { get; set; } 
    }

    public enum Periodicidad
    {
        Hora,
        Dia,
        Mes,
        Anio
    }
}
