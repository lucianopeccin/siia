﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XHouse.Model.Aparatos;

namespace XHouse.Model.Escenas
{
    /// <summary>
    /// Clase que representa los estados de los Aparatos asociados a la Escena
    /// </summary>
    [Table("EscenaAparatoEstado")]
    public class EscenaAparatoEstado : Identity
    {
        /// <summary>
        /// Id de la Escena a la que pertenece el Trigger
        /// </summary>
        [Required(ErrorMessage = "La Escena asociada al Trigger es obligatoria")]
        public int EscenaId { get; set; }

        /// <summary>
        /// Escena a la que pertenece el Trigger
        /// </summary>
        public virtual Escena Escena { get; set; }

        /// <summary>
        /// Id del Aparato asociado al Trigger
        /// </summary>
        [Required(ErrorMessage = "El Aparato asociado al Trigger es obligatorio")]
        public int AparatoId { get; set; }

        /// <summary>
        /// Aparato asociado al Trigger
        /// </summary>
        [ForeignKey("AparatoId")]
        public virtual Aparato Aparato { get; set; }

        /// <summary>
        /// Estado del aparato en el que se acciona el Trigger
        /// </summary>
        [Required(ErrorMessage = "El estado del Aparato es obligatorio")]
        public int Estado { get; set; }
    }
}
