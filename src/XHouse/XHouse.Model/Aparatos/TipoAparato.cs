﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHouse.Model.Aparatos
{
    [Table("TipoAparato")]
    public class TipoAparato : Identity
    {
        [Required(ErrorMessage = "El campo Nombre es requerido.", AllowEmptyStrings = false)]
        [MaxLength(100, ErrorMessage = "El Nombre no puede superar los 100 caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [MaxLength(300, ErrorMessage = "La Descripción no puede superar los 300 caracteres.")]
        public string Descripcion { get; set; }

        /// <summary>
        /// Indica si el Tipo de Aparato está habilitado
        /// </summary>
        [Display(Name = "Habilitado")]
        public bool Habilitado { get; set; }
        
        [Required(ErrorMessage = "El tipo de conexión es obligatorio")]
        [Display(Name = "Tipo de conexión")]
        public TipoConexion Conexion { get; set; }

        [Required(ErrorMessage = "El tope inferior del rango es obligatorio")]
        [Display(Name = "Tope inferior del rango")]
        public int RangoTopeInferior { get; set; }

        [Required(ErrorMessage = "El tope superior del rango es obligatorio")]
        [Display(Name = "Tope superior del rango")]
        public int RangoTopeSuperior { get; set; }

        /// <summary>
        /// Indica si el tipo de aparato puede o no apagarse
        /// </summary>
        [Display(Name = "Puede apagarse?")]
        public bool PuedeApagarse { get; set; }

        /// <summary>
        /// Indica el valor dentro del rango en el que el aparato está apagado.
        /// Este valor tiene sentido solo si el aparato puede apagarse.
        /// TODO: Validar que esté entre el rango superior y el rango inferior.
        /// </summary>
        [Display(Name = "Valor de apagado")]
        public int EstadoApagado { get; set; }

        [Required(ErrorMessage = "El tipo de operación del aparato es obligatorio")]
        [Display(Name = "Tipo de aparato")]
        public TipoOperacion Operacion { get; set; }

        public int? ImagenId { get; set; }

        [ForeignKey("ImagenId")]
        public Modulo Imagen { get; set; }
    }


    /// <summary>
    /// Indica el tipo de operación del aparato.
    /// Hay básicamente 2 tipos: Binarios y Graduales.
    /// </summary>
    public enum TipoOperacion
    {
        /// <summary>
        /// Aparato Binario
        /// </summary>
        Binario = 0,

        /// <summary>
        /// Aparato Gradual
        /// </summary>
        Gradual = 1
    }


    public enum TipoConexion
    {
        Entrada = 0,
        Salida = 1,
        Persiana = 2
    }
}