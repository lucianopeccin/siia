﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XHouse.Model.Message
{
    /// <summary>
    /// The message 18.
    /// </summary>
    public class Message18
    {
        /// <summary>
        /// 2 byte de numero de serie escrito en hexadecimal.
        /// </summary>
        public string serie { get; set; }

        /// <summary>
        /// 1 byte de cabecera escrito en hexadecimal.
        /// </summary>
        public string header { get; set; }

        /// <summary>
        /// 1 byte de numero de modulo escrito en hexadecimal.
        /// </summary>
        public string module { get; set; }

        /// <summary>
        /// 1 byte de numero de puerto escrito en hexadecimal.
        /// </summary>
        public string port { get; set; }

        /// <summary>
        /// 1 byte de numero de aparato dentro del puerto escrito en hexadecimal.
        /// </summary>
        public string aparato { get; set; }

        /// <summary>
        /// 1 byte de dato escrito en hexadecimal.
        /// </summary>
        public string data { get; set; }

        public string allData
        {
            get
            {
                return this.serie + this.header + this.module + this.port + this.aparato + this.data;
            }
        }
    }

    
}
