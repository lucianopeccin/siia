﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHouse.Model
{
    public class CamaraIp : Identity
    {
        [Required]
        [MaxLength(50, ErrorMessage = "El Nombre no puede superar los 50 caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [MaxLength(300, ErrorMessage = "La Descripción no puede superar los 300 caracteres.")]
        public string Descripcion { get; set; }

        [Required]
        [Display(Name = "Dirección IP")]
        [MaxLength(300, ErrorMessage = "La dirección IP no puede superar los 15 caracteres.")]
        public string DireccionIp { get; set; }

        [Display(Name = "Puerto")]
        [Required]
        [Range(0, 65535, ErrorMessage = "El número del puerto debe estar en el rango 0-65535")]
        public int Puerto { get; set; }
    }
}