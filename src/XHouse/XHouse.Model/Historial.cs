﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHouse.Model
{
    [Table("Historial")]
    public class Historial : Identity
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        [Display(Name = "Usuario")]
        public string UsuarioNombre { get; set; }

        public int ModuloId { get; set; }

        [Display(Name = "Módulo")]
        public string ModuloNombre { get; set; }

        public int AparatoId { get; set; }

        [Display(Name = "Aparato")]
        public string AparatoNombre { get; set; }

        [Required]
        [Display(Name = "Acción")]
        public string Accion { get; set; }

        [Required]
        public DateTime Fecha { get; set; }
    }
}