﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHouse.Model
{
    /// <summary>
    /// Clase abstracta que solo contiene el identificador.
    /// Todas las clases que se persisten en la base de datos deberían derivar de esta clase.
    /// </summary>
    public abstract class Identity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}
