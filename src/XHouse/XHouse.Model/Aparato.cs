﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace XHouse.Model
{
    [Table("Aparato")]
    public class Aparato : Identity
    {
        [Required(ErrorMessage = "El campo Nombre es requerido.")]
        [MaxLength(50, ErrorMessage = "El Nombre no puede superar los 50 caracteres.")]
        public string Nombre { get; set; }

        [Display(Name = "Descripción")]
        [MaxLength(300, ErrorMessage = "La Descripción no puede superar los 300 caracteres.")]
        public string Descripcion { get; set; }

        [Required(ErrorMessage = "Seleccione una zona válida.")]
        public int? ZonaId { get; set; }

        [ForeignKey("ZonaId")]
        public virtual Zona Zona { get; set; }

        //[Required(ErrorMessage = "Seleccione el modulo al que se encuentra conectado el aparato.")]
        //public int? ModuloId { get; set; }

        //[ForeignKey("ModuloId")]
        //public Modulo Modulo { get; set; }
    }
}