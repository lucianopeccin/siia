﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XHouse.Model
{
    [Table("Imagen")]
    public class Imagen : Identity
    {
        private const string FileNameFormat = "{0}-{1}-{2}";

        [Required]
        public ImagenTipoObjeto TipoObjeto { get; set; }

        [Required]
        public int IdObjeto { get; set; }

        [Required]
        public string NombreOriginal { get; set; }

        [Required]
        public string Path { get; set; }

        public Imagen()
        {   
        }

        public Imagen(ImagenTipoObjeto tipoObjeto, int idObjeto, string archivoNombre)
        {
            TipoObjeto = tipoObjeto;
            IdObjeto = idObjeto;
            NombreOriginal = archivoNombre;

            Path = string.Format(FileNameFormat, (int) tipoObjeto, idObjeto, archivoNombre);
        }
    }

    public enum ImagenTipoObjeto
    {
        TipoAparato = 1,
        Ambiente = 2
    }
}
