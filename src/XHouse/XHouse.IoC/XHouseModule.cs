﻿using Ninject.Modules;

using SerialPortListener;

using XHouse.BusinessLayer;
using XHouse.BusinessLayer.Interface;
using XHouse.Data;
using XHouse.Data.Interfaces;
using XHouse.MessageQueueEngine;
using XHouse.MessageQueueEngine.Interfaces;
using XHouse.Model.Escenas;
using XHouse.Service;
using XHouse.Service.Interface;

using XHouseWeb.Helpers;

namespace XHouse.IoC
{
    public class XHouseModule : NinjectModule
    {
        /// <summary>
        /// Setea los bindings entre las distintas dependencias y
        /// las clases con que se van a resolver dichas dependencias.
        /// </summary>
        public override void Load()
        {
            IDatabaseFactory databaseFactory = new DatabaseFactory();
            Bind(typeof(IDatabaseFactory)).ToConstant(databaseFactory);

            Bind(typeof(IRepository<>))
                  .To(typeof(Repository<>))
                  .WithConstructorArgument("dbFactory", databaseFactory);

            Bind(typeof(ImageProvider)).To(typeof(ImageProvider)).InSingletonScope();
            Bind(typeof(ISerialPortHelper)).To(typeof(SerialPortHelper)).InSingletonScope();
            Bind(typeof(IEngineMessage)).To(typeof(EngineMessage)).InSingletonScope();
            Bind(typeof(IMessageManager)).To(typeof(MessageManager)).InSingletonScope();
            Bind(typeof(IInbox)).To(typeof(Inbox)).InSingletonScope();
            Bind(typeof(IOutbox)).To(typeof(Outbox)).InSingletonScope();
            Bind(typeof(IMessageProcessor)).To(typeof(MessageProcessor));
            Bind(typeof(IEscenaService)).To(typeof(EscenaService));
            Bind(typeof(IAparatoService)).To(typeof(AparatoService));

            
        }
    }

    
}
