﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialConector
{
    public interface ISerialConectorFacade
    {
        bool PortIsOpen();

        bool StartListening();

        bool StopListening();
    }
}
