﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SerialPortListener;

namespace SerialConector
{
    public class SerialConector : ISerialConectorFacade
    {
        private SerialReader _serialReader;
        public SerialConector()
        {
            _serialReader = new SerialReader();
        }


        public bool PortIsOpen()
        {
            return _serialReader.IsPortOpen();
        }

        public bool StartListening()
        {
            try
            {
                _serialReader.StartListening();
                return true;
            }catch(Exception)
            {
                return false;
            }
        }

        public bool StopListening()
        {
            try
            {
                _serialReader.StopListening();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
